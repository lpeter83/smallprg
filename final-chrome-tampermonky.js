// ==UserScript==
// @name        autorefresh
// @namespace   autorefresh
// @match       http://www.apple.com/hk-zh/shop/buy-iphone/iphone6s/4.7-%E5%90%8B%E8%9E%A2%E5%B9%95-16gb-%E9%8A%80%E8%89%B2
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @version     1
// @grant       none
// ==/UserScript==


$(window).load(
    function() {

        var button = $('.as-purchaseinfo-button button[disabled=disabled]');
        if (button.length == 0) { 
		button.click();
        } else {
            location.href = location.href;   
        }  
    }
);

