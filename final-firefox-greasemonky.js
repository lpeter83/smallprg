// ==UserScript==
// @name        autorefresh
// @namespace   autorefresh
// @include     http://wiki.greasespot.net/Greasemonkey_Manual:Script_Management
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @version     1
// @grant       none
// ==/UserScript==
$(window).load(function () {
  var button = $('.as-purchaseinfo-button button[disabled=disabled]');
  if (button.length == 0) {
    button.click();
  } else {
    location.href = location.href;
  }
}
);
